// import colors from 'vuetify/es5/util/colors'
// import vuetifyModule from 'vuetify/src'

// const VuetifyLoaderPlugin = require('vuetify-loader/lib/plugin')
const pkg = require('./package')

module.exports = {
  mode: 'universal',
  // dev: (process.env.NODE_ENV !== 'production'),
  /*
  ** Headers of the page
  */
  head: {
    title: pkg.name,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: pkg.description }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      {
        rel: 'stylesheet',
        href:
          'https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons'
      }
    ]
  },

  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },

  /*
  ** Global CSS
  */
  // css: [
  //   '~/assets/style/app.styl'
  // ],

  /*
  ** Plugins to load before mounting the App
  */
  // plugins: [
  //   '@/plugins/vuetify'
  // ],

  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/pwa'
  ],
  /*
  ** Axios module configuration
  */
  axios: {
    // See https://github.com/nuxt-community/axios-module#options
  },

  buildModules: [
    // With options
    ['@nuxtjs/vuetify', { /* module options */ 
      dark: true,
      themes: {
        light: {
          primary: '#1BB934', // Primary Green
          accent: '#222C3C', // Perusian Variasion
          secondary: '#DFE2D2', // Light Green Accent
          perussian: '#212531', //  Color 05 B | {name: 'Prussian Blue', hex: '#212531', rgb: '2,18,83', hsl: '228,97%, 32%', CMYK: '31,25,0,68'}
          cerulean: '#18A4E0', // Color 04 A | {name: 'Bright Cerulean', hex: '#18A4E0', rgb: '24,164,224', hsl: '198,80.6%, 48.6%', CMYK: '89,27,0,12'}
          walls: '#EFF2F4', // Color 03 A | {name: 'Walls of Santorini', hex: '#EFF2F4', rgb: '239,242,244', hsl: '204,0.19%, 0.95%', CMYK: '2,1,0,4'}
          domed: '#0253B3', // Color 01 A | {name: 'Blue Domed Church', hex: '#0253B3', rgb: '2,83,179', hsl: '212.5,97.8%, 35.5%', CMYK: '99,54,0,30'}
          darktone: '#1e202c',
          // info: colors.teal.lighten1,
          // warning: colors.amber.base,
          // error: colors.deepOrange.accent4,
          // success: colors.green.accent3
        },
        dark: {
          primary: '#1BB934', // Primary Green
          accent: '#222C3C', // Perusian Variasion
          secondary: '#DFE2D2', // Light Green Accent
          perussian: '#212531', //  Color 05 B | {name: 'Prussian Blue', hex: '#212531', rgb: '2,18,83', hsl: '228,97%, 32%', CMYK: '31,25,0,68'}
          cerulean: '#18A4E0', // Color 04 A | {name: 'Bright Cerulean', hex: '#18A4E0', rgb: '24,164,224', hsl: '198,80.6%, 48.6%', CMYK: '89,27,0,12'}
          walls: '#EFF2F4', // Color 03 A | {name: 'Walls of Santorini', hex: '#EFF2F4', rgb: '239,242,244', hsl: '204,0.19%, 0.95%', CMYK: '2,1,0,4'}
          domed: '#0253B3', // Color 01 A | {name: 'Blue Domed Church', hex: '#0253B3', rgb: '2,83,179', hsl: '212.5,97.8%, 35.5%', CMYK: '99,54,0,30'}
          darktone: '#1e202c',
          // info: colors.teal.lighten1,
          // warning: colors.amber.base,
          // error: colors.deepOrange.accent4,
          // success: colors.green.accent3
        }
      },
    }]
  ],

  /*
  ** Build configuration
  */
  build: {
    // transpile: ['vuetify/lib'],
    // plugins: [new VuetifyLoaderPlugin()],
    // loaders: {
    //   stylus: {
    //     import: ['~assets/style/variables.styl']
    //   }
    // },

    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {
      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/,
          options: {
            fix: true
          }
        })
      }
    }
  }
}
